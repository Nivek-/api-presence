const db = require('../config/database')
const Forge = db.Forge
const Schooling = db.Schooling
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all schooling
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Schooling.findAll({
                        include: [{
                            model: Forge,
                            as: "forges",
                            through: {
                                attributes: [],
                              }
                        }]
                    })
                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one schooling with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Schooling.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                            model: Forge,
                            as: "forges",
                            through: {
                                attributes: [],
                              }
                        }]
                    })
                    .then((result) => {
                        // Show schooling if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new schooling
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let name = req.body.name

                // Verify parameters
                if (name != undefined && name.trim() != '') {

                    name = name.trim()

                    return Schooling.findAll({
                            where: {
                                name: name
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if name exist on database, create new schooling if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.nameAlreadyTaken)
                                reject(error)
                            } else {
                                return Schooling.create({
                                        name: name,
                                    })
                                    .then(() => {
                                        // Return created schooling to show it
                                        return Schooling.findAll({
                                            where: {
                                                name: name
                                            },
                                            include: [{
                                                model: Forge,
                                                as: "forges",
                                                through: {
                                                    attributes: [],
                                                  }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one schooling with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let name = req.body.name
                let id = req.params.id

                // Verify parameters sent in POST
                if (name != undefined && name.trim() != '') {

                    name = name.trim()

                    // Get schooling with sent ID
                    Schooling.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if name isn't already taken with other ID
                            if (result[0] != undefined) {
                                Schooling.findAll({
                                        where: {
                                            name: name,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if name is already taken, update schooling if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.nameAlreadyTaken)
                                            reject(error)
                                        } else {
                                            Schooling.update({
                                                    name: name,
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete schooling
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get schooling with id sent in GET
                Schooling.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete schooling if ID exist, show error if not
                        if (result[0] != undefined) {
                            Schooling.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    }

}