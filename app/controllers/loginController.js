const db = require('../config/database')
const Staff = db.Staff
const Learner = db.Learner

const config = require('../config/config')
const bcrypt = require("bcrypt")

module.exports = {
    login: (req, res) => {
        return new Promise((next) => {
            let error, user

            // Get parameters sent in POST
            let email = req.body.email
            let password = req.body.password

            // Verify parameters
            if ((email != undefined && password != undefined) && (email.trim() != '' && password.trim() != '')) {

                email = email.trim()
                password = password.trim()

                // Check if email exists in Staff table
                return Staff.findAll({
                        where: {
                            email: email
                        },
                        raw: true
                    })
                    .then((result) => {
                        // If email is found, fetched user is saved after password verification
                        if (result[0] != undefined) {
                            user = result[0]

                            return bcrypt.compare(password, result[0].password)
                                .then((result) => {

                                    if (result) {
                                        next(user)
                                    } else {
                                        error = new Error(config.errors.incorrectEmailOrPassword)
                                        next(error)
                                    }
                                })

                        // If not, check if email exists in Learner table
                        } else {
                            return Learner.findAll({
                                    where: {
                                        email: email
                                    },
                                    raw: true
                                })
                                .then((result) => {
                                     // If email is found, fetched user is saved after password verification
                                    if (result[0] != undefined) {
                                        user = result[0]
                                        
                                        return bcrypt.compare(password, result[0].password)
                                            .then((result) => {
                                                if (result) {
                                                    next(user)
                                                } else {
                                                    error = new Error(config.errors.incorrectEmailOrPassword)
                                                    next(error)
                                                }
                                            })
                                    } else {
                                        error = new Error(config.errors.incorrectEmailOrPassword)
                                        next(error)
                                    }
                                })
                        }
                    })
                    .catch((err) => next(err))
            } else {
                error = new Error(config.errors.missingParameter)
                next(error)
            }
        })
    }
}