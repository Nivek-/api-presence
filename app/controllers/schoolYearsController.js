const db = require('../config/database')
const SchoolYear = db.SchoolYear
const Learner = db.Learner
const Schooling = db.Schooling
const Forge = db.Forge
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all school years
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                SchoolYear.findAll({
                        include: [{
                                model: Learner,
                                as: "learners",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Schooling,
                                as: "schooling",
                                include: [{
                                    model: Forge,
                                    as: "forges",
                                    through: {
                                        attributes: [],
                                    }
                                }]
                            }
                        ]
                    })
                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one school year with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                SchoolYear.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                                model: Learner,
                                as: "learners",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Schooling,
                                as: "schooling",
                                include: [{
                                    model: Forge,
                                    as: "forges",
                                    through: {
                                        attributes: [],
                                    }
                                }]
                            }
                        ]
                    })
                    .then((result) => {
                        // Show school year if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new school year
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let name = req.body.name
                let start_date = req.body.start_date
                let end_date = req.body.end_date
                let schooling_id = req.body.schooling_id
                let forge_id = req.body.forge_id
                
                // Verify parameters
                if ((name != undefined && start_date != undefined && end_date != undefined && schooling_id != undefined && forge_id != undefined) && (name.trim() != '' && start_date.trim() != '' && end_date.trim() != '' && schooling_id.trim() != '' && forge_id.trim() != '')) {

                    name = name.trim()
                    start_date = start_date.trim()
                    end_date = end_date.trim()
                    schooling_id = schooling_id.trim()
                    forge_id = forge_id.trim()

                    return SchoolYear.findAll({
                            where: {
                                name: name
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if name exist on database, create new school year if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.nameAlreadyTaken)
                                reject(error)
                            } else {
                                return SchoolYear.findAll({
                                    attributes: ["number"],
                                    where: {
                                        forge_id: forge_id,
                                        schooling_id: schooling_id,
                                    },
                                    order:[ ['number', 'DESC']],
                                    limit: 1
                                })
                                    .then((count) => {
                                        let number
                                        if(result[0] != undefined) {
                                            number = count[0].number + 1
                                        } else {
                                            number = 1
                                        }

                                        return SchoolYear.create({
                                            name: name,
                                            start_date: start_date,
                                            end_date: end_date,
                                            schooling_id: schooling_id,
                                            number: number,
                                            forge_id: forge_id
                                        })
                                    })
                                    .then(() => {
                                        // Return created school year to show it
                                        return SchoolYear.findAll({
                                            where: {
                                                name: name
                                            },
                                            include: [{
                                                model: Schooling,
                                                as: "schooling",
                                                include: [{
                                                    model: Forge,
                                                    as: "forges",
                                                    where: {
                                                        id: 1
                                                    },
                                                    through: {
                                                        attributes: [],
                                                    }
                                                }]
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one school year with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let name = req.body.name
                let start_date = req.body.start_date
                let end_date = req.body.end_date
                let schooling_id = schooling_id
                let id = req.params.id

                // Verify parameters sent in POST
                if ((name != undefined && start_date != undefined && end_date != undefined && schooling_id != undefined) && (name.trim() != '' && start_date.trim() != '' && end_date.trim() != '' && schooling_id.trim() != '')) {

                    name = name.trim()
                    start_date = start_date.trim()
                    end_date = end_date.trim()
                    schooling_id = schooling_id.trim()

                    // Get learner with sent ID
                    SchoolYear.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if name isn't already taken with other ID
                            if (result[0] != undefined) {
                                SchoolYear.findAll({
                                        where: {
                                            name: name,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if name is already taken, update school year if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.nameAlreadyTaken)
                                            reject(error)
                                        } else {
                                            SchoolYear.update({
                                                    name: name,
                                                    start_date: start_date,
                                                    end_date: end_date,
                                                    schooling_id: schooling_id
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete school year
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get school year with id sent in GET
                SchoolYear.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete school year if ID exist, show error if not
                        if (result[0] != undefined) {
                            SchoolYear.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // associate one learner with one school_year 
    addLearner: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let learner = req.body.learner_id
                let school_year = req.params.id
                if (learner != undefined && learner.trim() != '') {

                    learner = learner.trim()

                    return Learner.findAll({
                            where: {
                                id: learner
                            }
                        })
                        .then((result) => {
                            // Check if learner id exist, show error if not
                            if (result[0] != undefined) {
                                return SchoolYear.findAll({
                                        where: {
                                            id: school_year
                                        }
                                    })
                                    .then((result) => {
                                        // Add learner to school_year if ID exist, show error if not 
                                        if (result[0] != undefined) {
                                            return result[0].addLearner(learner)
                                                .then(() => {
                                                    return SchoolYear.findAll({
                                                        where: {
                                                            id: school_year
                                                        },
                                                        include: [{
                                                            model: Learner,
                                                            as: "learners",
                                                            through: {
                                                                attributes: [],
                                                            }
                                                        }]
                                                    })
                                                })
                                                .then((result) => {
                                                    resolve(res.json(checkAndChange(result[0])))
                                                })
                                        } else {
                                            let error = new Error(config.errors.wrongID)
                                            reject(error)
                                        }
                                    })

                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)

                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Dissociate one learner to one school_year
    removeLearner: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let learner = req.body.learner_id
                let school_year = req.params.id
                if (learner != undefined && learner.trim() != '') {

                    learner = learner.trim()

                    return SchoolYear.findAll({
                            where: {
                                '$learners.id$': learner,
                                id: school_year
                            },
                            include: [{
                                model: Learner,
                                as: "learners",
                                through: {
                                    attributes: [],
                                }
                            }]
                        })
                        .then((result) => {
                            // Remove learner to school_year if learner ID and school_year ID exists, show error if not
                            if (result[0] != undefined) {
                                return result[0].removeLearner(learner)
                                    .then(() => {
                                        return SchoolYear.findAll({
                                            where: {
                                                id: school_year
                                            },
                                            include: [{
                                                model: Learner,
                                                as: "learners",
                                                through: {
                                                    attributes: [],
                                                }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    }
}