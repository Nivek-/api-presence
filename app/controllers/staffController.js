const db = require('../config/database')
const Forge = db.Forge
const Staff = db.Staff
const Skill = db.Skill
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')
const bcrypt = require('bcrypt')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all staff members
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Staff.findAll({
                        include: [{
                                model: Forge,
                                as: "forges",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Skill,
                                as: "skills",
                                through: {
                                    attributes: [],
                                }
                            }
                        ]
                    })
                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one staff member with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Staff.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                                model: Forge,
                                as: "forges",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Skill,
                                as: "skills",
                                through: {
                                    attributes: [],
                                }
                            }
                        ]
                    })
                    .then((result) => {
                        // Show staff member if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new staff member
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let firstname = req.body.firstname
                let lastname = req.body.lastname
                let email = req.body.email
                let role = req.body.role

                // Verify parameters
                if ((firstname != undefined && lastname != undefined && email != undefined && role != undefined) && (firstname.trim() != '' && lastname.trim() != '' && email.trim() != '' && role.trim() != '')) {

                    firstname = firstname.trim()
                    lastname = lastname.trim()
                    email = email.trim()
                    role = role.trim()

                    return Staff.findAll({
                            where: {
                                email: email
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if email exist on database, create new staff member if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.emailAlreadyTaken)
                                reject(error)
                            } else {
                                // crypt password before saving it in database
                                let password = firstname.charAt(0) + lastname
                                return bcrypt.hash(password.toLowerCase(), 10)
                                    .then((hashed_password) => {
                                        return Staff.create({
                                            firstname: firstname,
                                            lastname: lastname,
                                            email: email,
                                            role: role,
                                            password: hashed_password
                                        })
                                    })

                                    .then(() => {
                                        // Return created staff member to show it
                                        return Staff.findAll({
                                            where: {
                                                email: email
                                            },
                                            include: [{
                                                    model: Forge,
                                                    as: "forges",
                                                    through: {
                                                        attributes: [],
                                                    }
                                                },
                                                {
                                                    model: Skill,
                                                    as: "skills",
                                                    through: {
                                                        attributes: [],
                                                    }
                                                }
                                            ]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one staff member with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let firstname = req.body.firstname
                let lastname = req.body.lastname
                let email = req.body.email
                let role = req.body.role
                let id = req.params.id

                // Verify parameters sent in POST
                if ((firstname != undefined && lastname != undefined && email != undefined && role != undefined) && (firstname.trim() != '' && lastname.trim() != '' && email.trim() != '' && role.trim() != '')) {

                    firstname = firstname.trim()
                    lastname = lastname.trim()
                    email = email.trim()
                    role = role.trim()

                    // Get staff with sent ID
                    Staff.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if email isn't already taken with other ID
                            if (result[0] != undefined) {
                                Staff.findAll({
                                        where: {
                                            email: email,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if email is already taken, update staff member if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.emailAlreadyTaken)
                                            reject(error)
                                        } else {
                                            Staff.update({
                                                    firstname: firstname,
                                                    lastname: lastname,
                                                    email: email,
                                                    role: role
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete staff member
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get staff member with id sent in GET
                Staff.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete staff member if ID exist, show error if not
                        if (result[0] != undefined) {
                            Staff.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // associate one skill with one staff member 
    addSkill: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let skill = req.body.skill_id
                let staff = req.params.id
                if (skill != undefined && skill.trim() != '') {

                    skill = skill.trim()

                    return Skill.findAll({
                            where: {
                                id: skill
                            }
                        })
                        .then((result) => {
                            // Check if skill id exist, show error if not
                            if (result[0] != undefined) {
                                return Staff.findAll({
                                        where: {
                                            id: staff
                                        }
                                    })
                                    .then((result) => {
                                        // Add skill to staff member if ID exist, show error if not 
                                        if (result[0] != undefined) {
                                            return result[0].addSkill(skill)
                                                .then(() => {
                                                    return Staff.findAll({
                                                        where: {
                                                            id: staff
                                                        },
                                                        include: [{
                                                            model: Skill,
                                                            as: "skills",
                                                            through: {
                                                                attributes: [],
                                                            }
                                                        }]
                                                    })
                                                })
                                                .then((result) => {
                                                    resolve(res.json(checkAndChange(result[0])))
                                                })
                                        } else {
                                            let error = new Error(config.errors.wrongID)
                                            reject(error)
                                        }
                                    })

                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)

                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Dissociate one skill to one staff member
    removeSkill: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let skill = req.body.skill_id
                let staff = req.params.id
                if (skill != undefined && skill.trim() != '') {

                    skill = skill.trim()

                    return Staff.findAll({
                            where: {
                                '$skills.id$': skill,
                                id: staff
                            },
                            include: [{
                                model: Skill,
                                as: "skills",
                                through: {
                                    attributes: [],
                                }
                            }]
                        })
                        .then((result) => {
                            // Remove skill to staff member if skill ID and staff member ID exists, show error if not
                            if (result[0] != undefined) {
                                return result[0].removeSkill(skill)
                                    .then(() => {
                                        return Staff.findAll({
                                            where: {
                                                id: staff
                                            },
                                            include: [{
                                                model: Skill,
                                                as: "skills",
                                                through: {
                                                    attributes: [],
                                                }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

}