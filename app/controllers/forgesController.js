const db = require('../config/database')
const Forge = db.Forge
const Schooling = db.Schooling
const Staff = db.Staff
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all forges
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Forge.findAll({
                        include: [{
                                model: Schooling,
                                as: "schooling",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Staff,
                                as: "staff",
                                through: {
                                    attributes: [],
                                }
                            }
                        ]
                    })

                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one forge with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Forge.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                                model: Schooling,
                                as: "schooling",
                                through: {
                                    attributes: [],
                                }
                            },
                            {
                                model: Staff,
                                as: "staff",
                                through: {
                                    attributes: [],
                                }
                            }
                        ]
                    })
                    .then((result) => {
                        // Show forge if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new forge
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let name = req.body.name

                // Verify parameters
                if (name != undefined && name.trim() != '') {

                    name = name.trim()


                    Forge.findAll({
                            where: {
                                name: name
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if name exist on database, create new forge if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.nameAlreadyTaken)
                                reject(error)
                            } else {
                                Forge.create({
                                        name: name,
                                    })
                                    .then(() => {
                                        // Return created forge to show it
                                        return Forge.findAll({
                                            where: {
                                                name: name
                                            },
                                            include: [{
                                                    model: Schooling,
                                                    as: "schooling",
                                                    through: {
                                                        attributes: [],
                                                    }
                                                },
                                                {
                                                    model: Staff,
                                                    as: "staff",
                                                    through: {
                                                        attributes: [],
                                                    }
                                                }
                                            ]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one forge with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let name = req.body.name
                let id = req.params.id

                // Verify parameters sent in POST
                if (name != undefined && name.trim() != '') {

                    name = name.trim()

                    // Get forge with sent ID
                    Forge.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if name isn't already taken with other ID
                            if (result[0] != undefined) {
                                Forge.findAll({
                                        where: {
                                            name: name,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if name is already taken, update forge if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.nameAlreadyTaken)
                                            reject(error)
                                        } else {
                                            Forge.update({
                                                    name: name,
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete forge
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get forge with id sent in GET
                Forge.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete forge if ID exist, show error if not
                        if (result[0] != undefined) {
                            Forge.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // associate one schooling with one forge 
    addSchooling: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let schooling = req.body.schooling_id
                let forge = req.params.id
                if (schooling != undefined && schooling.trim() != '') {

                    schooling = schooling.trim()

                    return Schooling.findAll({
                            where: {
                                id: schooling
                            }
                        })
                        .then((result) => {
                            // Check if schooling id exist, show error if not
                            if (result[0] != undefined) {
                                return Forge.findAll({
                                        where: {
                                            id: forge
                                        }
                                    })
                                    .then((result) => {
                                        // Add schooling to forge if ID exist, show error if not 
                                        if (result[0] != undefined) {
                                            return result[0].addSchooling(schooling)
                                                .then(() => {
                                                    return Forge.findAll({
                                                        where: {
                                                            id: forge
                                                        },
                                                        include: [{
                                                            model: Schooling,
                                                            as: "schooling",
                                                            through: {
                                                                attributes: [],
                                                            }
                                                        }]
                                                    })
                                                })
                                                .then((result) => {
                                                    resolve(res.json(checkAndChange(result[0])))
                                                })
                                        } else {
                                            let error = new Error(config.errors.wrongID)
                                            reject(error)
                                        }
                                    })

                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)

                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Dissociate one schooling to one forge
    removeSchooling: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let schooling = req.body.schooling_id
                let forge = req.params.id
                if (schooling != undefined && schooling.trim() != '') {

                    schooling = schooling.trim()

                    return Forge.findAll({
                            where: {
                                '$schooling.id$': schooling,
                                id: forge
                            },
                            include: [{
                                model: Schooling,
                                as: "schooling",
                                through: {
                                    attributes: [],
                                }
                            }]
                        })
                        .then((result) => {
                            // Remove schooling to forge if schooling ID and forge ID exists, show error if not
                            if (result[0] != undefined) {
                                return result[0].removeSchooling(schooling)
                                    .then(() => {
                                        return Forge.findAll({
                                            where: {
                                                id: forge
                                            },
                                            include: [{
                                                model: Schooling,
                                                as: "schooling",
                                                through: {
                                                    attributes: [],
                                                }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // associate one staff member with one forge 
    addStaff: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let staff = req.body.staff_id
                let forge = req.params.id
                if (staff != undefined && staff.trim() != '') {

                    staff = staff.trim()

                    return Staff.findAll({
                            where: {
                                id: staff
                            }
                        })
                        .then((result) => {
                            // Check if staff id exist, show error if not
                            if (result[0] != undefined) {
                                return Forge.findAll({
                                        where: {
                                            id: forge
                                        }
                                    })
                                    .then((result) => {
                                        // Add staff to forge if ID exist, show error if not 
                                        if (result[0] != undefined) {
                                            return result[0].addStaff(staff)
                                                .then(() => {
                                                    return Forge.findAll({
                                                        where: {
                                                            id: forge
                                                        },
                                                        include: [{
                                                            model: Staff,
                                                            as: "staff",
                                                            through: {
                                                                attributes: [],
                                                            }
                                                        }]
                                                    })
                                                })
                                                .then((result) => {
                                                    resolve(res.json(checkAndChange(result[0])))
                                                })
                                        } else {
                                            let error = new Error(config.errors.wrongID)
                                            reject(error)
                                        }
                                    })

                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)

                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Dissociate one staff to one forge
    removeStaff: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let staff = req.body.staff_id
                let forge = req.params.id
                if (staff != undefined && staff.trim() != '') {

                    staff = staff.trim()

                    return Forge.findAll({
                            where: {
                                '$staff.id$': staff,
                                id: forge
                            },
                            include: [{
                                model: Staff,
                                as: "staff",
                                through: {
                                    attributes: [],
                                }
                            }]
                        })
                        .then((result) => {
                            // Remove staff to forge if staff ID and forge ID exists, show error if not
                            if (result[0] != undefined) {
                                return result[0].removeStaff(staff)
                                    .then(() => {
                                        return Forge.findAll({
                                            where: {
                                                id: forge
                                            },
                                            include: [{
                                                model: Staff,
                                                as: "staff",
                                                through: {
                                                    attributes: [],
                                                }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            } else {
                                let error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })
                } else {
                    error = new Error(config.errors.noIDValue)
                    reject(error)
                }
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    }
}