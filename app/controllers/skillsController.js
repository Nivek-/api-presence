const db = require('../config/database')
const Skill = db.Skill
const Staff = db.Staff
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all skills
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Skill.findAll({
                        include: [{
                            model: Staff,
                            as: "staff",
                            through: {
                                attributes: [],
                              }
                        }]
                    })
                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one skill with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Skill.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                            model: Staff,
                            as: "staff",
                            through: {
                                attributes: [],
                              }
                        }]
                    })
                    .then((result) => {
                        // Show skill if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new skill
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let name = req.body.name

                // Verify parameters
                if (name != undefined && name.trim() != '') {

                    name = name.trim()

                    return Skill.findAll({
                            where: {
                                name: name
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if name exist on database, create new skill if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.nameAlreadyTaken)
                                reject(error)
                            } else {
                                return Skill.create({
                                        name: name
                                    })
                                    .then(() => {
                                        // Return created skill to show it
                                        return Skill.findAll({
                                            where: {
                                               name: name
                                            },
                                            include: [{
                                                model: Staff,
                                                as: "staff",
                                                through: {
                                                    attributes: [],
                                                  }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one skill with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get parameters sent in POST and GET
                let name = req.body.name
                let id = req.params.id

                // Verify parameters sent in POST
                if (name != undefined && name.trim() != '') {

                    name = name.trim()

                    // Get skill with sent ID
                    Skill.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if name isn't already taken with other ID
                            if (result[0] != undefined) {
                                Skill.findAll({
                                        where: {
                                            name: name,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if name is already taken, update skill if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.nameAlreadyTaken)
                                            reject(error)
                                        } else {
                                            Skill.update({
                                                    name: name,
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.noNameValue)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete skill
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get skill with id sent in GET
                Skill.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete skill if ID exist, show error if not
                        if (result[0] != undefined) {
                            Skill.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    }

}