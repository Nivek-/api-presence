const db = require('../config/database')
const Learner = db.Learner
const SchoolYear = db.SchoolYear
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')
const bcrypt = require('bcrypt')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all learners
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Learner.findAll({
                        include: [{
                            model: SchoolYear,
                            as: "schoolyears",
                            through: {
                                attributes: [],
                            }
                        }]
                    })
                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one learner with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Learner.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                            model: SchoolYear,
                            as: "schoolyears",
                            through: {
                                attributes: [],
                            }
                        }]
                    })
                    .then((result) => {
                        // Show learner if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new learner
    create: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST
                let firstname = req.body.firstname
                let lastname = req.body.lastname
                let email = req.body.email
                let birthdate = req.body.birthdate

                // Verify parameters
                if ((firstname != undefined && lastname != undefined && email != undefined && birthdate != undefined) && (firstname.trim() != '' && lastname.trim() != '' && email.trim() != '' && birthdate.trim() != '')) {

                    firstname = firstname.trim()
                    lastname = lastname.trim()
                    email = email.trim()
                    birthdate = birthdate.trim()

                    return Learner.findAll({
                            where: {
                                email: email
                            },
                            raw: true
                        })
                        .then((result) => {
                            // Show error if email exist on database, create new learner if not
                            if (result[0] != undefined) {
                                error = new Error(config.errors.emailAlreadyTaken)
                                reject(error)
                            } else {
                                // crypt password before saving it in database
                                let password = firstname.charAt(0) + lastname
                                return bcrypt.hash(password.toLowerCase(), 10)

                                    .then((hashed_password) => {
                                        return Learner.create({
                                            firstname: firstname,
                                            lastname: lastname,
                                            email: email,
                                            birthdate: birthdate,
                                            on_training: 0,
                                            password: hashed_password
                                        })
                                    })
                                    .then(() => {
                                        // Return created learner to show it
                                        return Learner.findAll({
                                            where: {
                                                email: email
                                            },
                                            include: [{
                                                model: SchoolYear,
                                                as: "schoolyears",
                                                through: {
                                                    attributes: [],
                                                }
                                            }]
                                        })
                                    })
                                    .then((result) => {
                                        resolve(res.json(checkAndChange(result[0])))
                                    })
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Update one learner with ID
    update: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let firstname = req.body.firstname
                let lastname = req.body.lastname
                let email = req.body.email
                let birthdate = req.body.birthdate
                let id = req.params.id

                // Verify parameters sent in POST
                if ((firstname != undefined && lastname != undefined && email != undefined && birthdate != undefined) && (firstname.trim() != '' && lastname.trim() != '' && email.trim() != '' && birthdate.trim() != '')) {

                    firstname = firstname.trim()
                    lastname = lastname.trim()
                    email = email.trim()
                    birthdate = birthdate.trim()

                    // Get staff with sent ID
                    Learner.findAll({
                            where: {
                                id: id
                            },
                            raw: true
                        })
                        .then((result) => {

                            // If ID exist, check if email isn't already taken with other ID
                            if (result[0] != undefined) {
                                Learner.findAll({
                                        where: {
                                            email: email,
                                            id: {
                                                [Op.ne]: id
                                            }
                                        },
                                        raw: true
                                    })
                                    .then((result) => {
                                        // Show error if email is already taken, update learner if not
                                        if (result[0] != undefined) {
                                            error = new Error(config.errors.emailAlreadyTaken)
                                            reject(error)
                                        } else {
                                            Learner.update({
                                                    firstname: firstname,
                                                    lastname: lastname,
                                                    email: email,
                                                    birthdate: birthdate
                                                }, {
                                                    where: {
                                                        id: id
                                                    }
                                                })
                                                .then(() => resolve(res.json(checkAndChange(true))))
                                        }
                                    })


                            } else {
                                // Show error if id doesn't exist
                                error = new Error(config.errors.wrongID)
                                reject(error)
                            }
                        })

                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Delete learner
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get learner with id sent in GET
                Learner.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete learner if ID exist, show error if not
                        if (result[0] != undefined) {
                            Learner.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    },


    // Change on_training status
    changeStatus: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Save actual date to check if school years is current
                let actual_date = Math.floor(Date.now() / 1000)

                // Find learners where schooling years are current
                return Learner.findAll({
                        attributes: ["id"],
                        where: {
                            '$schoolyears.start_date$': {
                                [Op.lt]: actual_date
                            },
                            '$schoolyears.end_date$': {
                                [Op.gt]: actual_date
                            },

                        },
                        include: [{
                            model: SchoolYear,
                            as: "schoolyears",
                            attributes: [],
                            through: {
                                attributes: [],
                            }
                        }],
                        raw: true
                    })
                    .then((result) => {
                        // Change status to 1 for each learner in a current schooling year
                        if (result[0] != undefined) {
                            result.forEach(learner => {
                                Learner.update({
                                    on_training: 1,
                                }, {
                                    where: {
                                        id: learner.id
                                    }
                                })
                            })
                            resolve(res.json(checkAndChange(true)))
                        } else {
                            resolve(res.json(checkAndChange(false)))
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    }
}