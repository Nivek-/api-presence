const db = require('../config/database')
const Call = db.Call
const Learner = db.Learner
const SchoolYear = db.SchoolYear
const {
    checkAndChange
} = require('../functions')
const config = require('../config/config')

// Operators for WHERE conditions used with sequelize
const {
    Op
} = require('sequelize')



module.exports = {
    // Get all calls
    findAll: (req, res) => {
        // Create new promise 
        new Promise((resolve, reject) => {
                Call.findAll({
                        include: [{
                                model: Learner
                            },
                            {
                                model: SchoolYear

                            }
                        ]
                    })

                    .then((result) => {
                        resolve(res.json(checkAndChange(result)))
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Get one call with ID
    findOne: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                Call.findAll({
                        where: {
                            id: req.params.id
                        },
                        include: [{
                            model: Learner
                        },
                        {
                            model: SchoolYear

                        }
                    ]
                    })
                    .then((result) => {
                        // Show call if ID exist, show error if not 
                        if (result[0] != undefined) {
                            resolve(res.json(checkAndChange(result[0])))
                        } else {
                            let error = new Error(config.errors.wrongID)
                            reject(error)

                        }
                    })
            })
            .catch((err) => {
                res.json(checkAndChange(err))
            })
    },

    // Add new call
    create: (date,morning,afternoon,lateness,comment, learner, schoolyear) => {
        // Create new promise
        return new Promise((next) => {
            Call.create({
                    date: date,
                    morning: morning,
                    afternoon: afternoon,
                    lateness: lateness,
                    comment: comment,
                    learner_id: learner,
                    school_year_id: schoolyear
                })
                .then(() => {
                    // Return created call to show it
                    return Call.findAll({
                        where: {
                            date: date,
                            learner_id: learner
                        }
                    })
                })
                .then((result) => {
                    next(checkAndChange(result[0]))
                })
        })




    },

    // Update one call with ID
    update: (date,morning,afternoon,lateness,comment,learner) => {
        // Create new promise
        return new Promise((next) => {

            Call.update({
                    morning: morning,
                    afternoon: afternoon,
                    lateness: lateness,
                    comment: comment
                }, {
                    where: {
                        date: date,
                        learner_id: learner
                    }
                })
                .then(() => {
                    next(checkAndChange(true))
                })
        })
    },


    // Delete call
    delete: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error

                // Get call with id sent in GET
                Call.findAll({
                        where: {
                            id: req.params.id
                        },
                        raw: true
                    })
                    .then((result) => {
                        // Delete call if ID exist, show error if not
                        if (result[0] != undefined) {
                            Call.destroy({
                                    where: {
                                        id: req.params.id
                                    }
                                })
                                .then(() => resolve(res.json(checkAndChange(true))))
                        } else {
                            error = new Error(config.errors.wrongID)
                            reject(error)
                        }
                    })
            })
            .catch((err) => res.json(checkAndChange(err)))
    },

    // Create or update call
    save: (req, res) => {
        // Create new promise
        new Promise((resolve, reject) => {
                let error
                // Get parameters sent in POST and GET
                let date = req.body.date
                let morning = req.body.morning
                let afternoon = req.body.afternoon
                let lateness = req.body.lateness
                let comment = req.body.comment
                let learner = req.body.learner_id
                let schoolyear = req.body.schoolyear_id
                
                // Verify parameters sent in POST
                if ((date != undefined && morning != undefined && afternoon != undefined && lateness != undefined && comment != undefined) && (date.trim() != '' && morning.trim() != '' && afternoon.trim() != '' && lateness.trim() != '')) {

                    date = parseInt(date)
                    morning = parseInt(morning)
                    afternoon = parseInt(afternoon)
                    lateness = parseInt(lateness)
                    comment = comment.trim()

                    Call.findAll({
                            where: {
                                date: date,
                                learner_id: learner
                            },
                            raw: true
                        })
                        .then((result) => {
                            // update call if date exist, create if not
                            if (result[0] == undefined) {
                                module.exports.create(date,morning,afternoon,lateness,comment, learner, schoolyear)
                                .then((result) => {
                                    resolve(res.json(result))
                                })
                            } else {
                                let id = req.body.id
                                module.exports.update(date,morning,afternoon,lateness,comment,learner)
                                .then((result) => {
                                    resolve(res.json(result))
                                })
                            }
                        })


                } else {
                    error = new Error(config.errors.missingParameter)
                    reject(error)
                }
            })
            .catch((err) => res.json(checkAndChange(err)))
    }
}