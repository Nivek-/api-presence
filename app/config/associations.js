const db = require('../config/database')
const Forge = db.Forge
const Schooling = db.Schooling
const Staff = db.Staff
const Skill = db.Skill
const Learner = db.Learner
const SchoolYear = db.SchoolYear
const Call = db.Call

// Association between Forge and Schooling
Forge.belongsToMany(Schooling, {
  through: "Forge_Schooling",
  as: 'schooling',
  foreignKey: 'forge_id',
  timestamps: false,
  onDelete: 'CASCADE'
})

Schooling.belongsToMany(Forge, {
  through: "Forge_Schooling",
  as: 'forges',
  foreignKey: 'schooling_id',
  timestamps: false,
  onDelete: 'CASCADE'
})


// Association between Forge and Staff
Forge.belongsToMany(Staff, {
  through: "Forge_Staff",
  as: 'staff',
  foreignKey: 'forge_id',
  timestamps: false,
  onDelete: 'CASCADE'
})

Staff.belongsToMany(Forge, {
  through: "Forge_Staff",
  as: 'forges',
  foreignKey: 'staff_id',
  timestamps: false,
  onDelete: 'CASCADE'
})


// Association between Staff and Skill
Staff.belongsToMany(Skill, {
  through: "Staff_Skill",
  as: 'skills',
  foreignKey: 'staff_id',
  timestamps: false,
  onDelete: 'CASCADE'
})

Skill.belongsToMany(Staff, {
  through: "Staff_Skill",
  as: 'staff',
  foreignKey: 'skill_id',
  timestamps: false,
  onDelete: 'CASCADE'
})


// Association between ScoolYear and Learner
SchoolYear.belongsToMany(Learner, {
  through: "SchoolYear_Learner",
  as: 'learners',
  foreignKey: 'schoolyear_id',
  timestamps: false,
  onDelete: 'CASCADE'
})

Learner.belongsToMany(SchoolYear, {
  through: "SchoolYear_Learner",
  as: 'schoolyears',
  foreignKey: 'learner_id',
  timestamps: false,
  onDelete: 'CASCADE'
})


// Association between ScoolYear and Schooling
SchoolYear.belongsTo(Schooling, {
  foreignKey: "schooling_id"
})

Schooling.hasMany(SchoolYear, {
  foreignKey: "schooling_id",
})


// Association between ScoolYear and Forge
SchoolYear.belongsTo(Forge, {
})

Forge.hasMany(SchoolYear, {
  foreignKey: "forge_id",
})


// Association between Call and Learner
Call.belongsTo(Learner, {
})

Learner.hasMany(Call, {
  foreignKey: "learner_id",
})


// Association between Call and Schoolyear
Call.belongsTo(SchoolYear, {
})

SchoolYear.hasMany(Call, {
  foreignKey: "school_year_id",
})