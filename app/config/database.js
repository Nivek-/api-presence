const config = require("./config")
const Sequelize = require("sequelize");
// Setup database with sequelize and confid file
const sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, {
  host: config.db.host,
  dialect: config.db.dialect,
  logging: false,
})

// Verify database connection and synchronise models with it
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .then(() => {
    // Require tables associations before synchronize database
    require("./associations")
    sequelize.sync()
  })
  .catch(err => {
    console.error(err);
  });

// save sequelize, database setup and models in variable to exports
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Database tables
db.Forge = require("../models/forge")(sequelize, Sequelize)
db.Schooling = require("../models/schooling")(sequelize, Sequelize)
db.Staff = require("../models/staff")(sequelize, Sequelize)
db.Skill = require("../models/skill")(sequelize, Sequelize)
db.Learner = require("../models/learner")(sequelize, Sequelize)
db.SchoolYear = require("../models/school_year")(sequelize, Sequelize)
db.Call = require("../models/call")(sequelize, Sequelize)

module.exports = db;