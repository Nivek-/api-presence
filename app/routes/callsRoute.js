module.exports = (router) => {
    const calls = require("../controllers/callsController");
    
    router.route('/')
      // Create a call
      .post(calls.save)
  
      // Get all calls
      .get(calls.findAll)
  
      
    router.route('/:id')
      // Get one call with ID
      .get(calls.findOne)
  
      // Update one call with ID
      .put(calls.save)
  
      // Delete one call with ID
      .delete(calls.delete)
  }