module.exports = (router) => {
    const school_years = require("../controllers/schoolYearsController");
    
    router.route('/')
      // Create a school_year
      .post(school_years.create)
  
      // Get all school_years
      .get(school_years.findAll)
  
    router.route('/:id')
      // Get one school_year with ID
      .get(school_years.findOne)
  
      // Update one school_year with ID
      .put(school_years.update)
  
      // Delete one school_year with ID
      .delete(school_years.delete)

    router.route("/learners/:id")
      // Add learner to one forge
      .post(school_years.addLearner)

      // Remove learner to one forge
      .put(school_years.removeLearner)
  }