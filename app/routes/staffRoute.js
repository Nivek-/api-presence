module.exports = (router) => {
    const staff = require("../controllers/staffController");
    
    router.route('/')
      // Create a staff member
      .post(staff.create)
  
      // Get all staff members
      .get(staff.findAll)
  

    router.route('/:id') 
      // Get one staff member with ID
      .get(staff.findOne)
  
      // Update one staff member with ID
      .put(staff.update)
  
      // Delete one staff member with ID
      .delete(staff.delete)


      router.route("/skills/:id")
      // Add skill to one staff member
      .post(staff.addSkill)

      // Remove skill to one staff member
      .put(staff.removeSkill)
  }