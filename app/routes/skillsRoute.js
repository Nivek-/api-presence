module.exports = (router) => {
    const skills = require("../controllers/skillsController");
    
    router.route('/')
      // Create a skill
      .post(skills.create)
  
      // Get all skills
      .get(skills.findAll)
  
    router.route('/:id') 
      // Get one skill with ID
      .get(skills.findOne)
  
      // Update one skill with ID
      .put(skills.update)
  
      // Delete one skill with ID
      .delete(skills.delete)
  }