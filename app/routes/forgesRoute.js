module.exports = (router) => {
    const forges = require("../controllers/forgesController");
    
    router.route('/')
      // Create a forge
      .post(forges.create)
  
      // Get all forges
      .get(forges.findAll)
  
      
    router.route('/:id')
      // Get one forge with ID
      .get(forges.findOne)
  
      // Update one forge with ID
      .put(forges.update)
  
      // Delete one forge with ID
      .delete(forges.delete)


    router.route("/schooling/:id")
      // Add schooling to one forge
      .post(forges.addSchooling)

      // Remove schooling to one forge
      .put(forges.removeSchooling)


    router.route("/staff/:id")
      // Add staff to one forge
      .post(forges.addStaff)

      // Remove staff to one forge
      .put(forges.removeStaff)

      
  }