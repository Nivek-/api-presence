module.exports = (router) => {
    const schooling = require("../controllers/schoolingController");
    
    router.route('/')
      // Create a schooling
      .post(schooling.create)
  
      // Get all schooling
      .get(schooling.findAll)
  
    router.route('/:id')
      // Get one schooling with ID
      .get(schooling.findOne)
  
      // Update one schooling with ID
      .put(schooling.update)
  
      // Delete one schooling with ID
      .delete(schooling.delete)
  }