module.exports = (router) => {
    const learners = require("../controllers/learnersController");

    router.route('/')
        // Create a learner
        .post(learners.create)

        // Get all learners
        .get(learners.findAll)


    router.route('/learner/:id')
        // Get one learner with ID
        .get(learners.findOne)

        // Update one learner with ID
        .put(learners.update)

        // Delete one learner with ID
        .delete(learners.delete)


    router.route('/status')
        // Change learner status    
        .get(learners.changeStatus)

}