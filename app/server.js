// Save modules and config in variables
const express = require('express')
const app = express()
const config = require('./config/config')
const session = require('express-session')
const {
    checkAndChange,
    isErr
} = require('./functions')
const auth = require("./controllers/loginController")


// Parsing results
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

// Configure session
app.use(session({
    secret: 'presence_app',
    resave: true,
    saveUninitialized: true
}))

// Setup path for all forges routes
let ForgesRouter = express.Router()
require("./routes/forgesRoute")(ForgesRouter);
app.use(config.rootAPI + 'forges', ForgesRouter)

// Setup path for all schooling routes
let SchoolingRouter = express.Router()
require("./routes/schoolingRoute")(SchoolingRouter);
app.use(config.rootAPI + 'schooling', SchoolingRouter)

// Setup path for all staff routes
let StaffRouter = express.Router()
require("./routes/staffRoute")(StaffRouter);
app.use(config.rootAPI + 'staff', StaffRouter)

// Setup path for all staff routes
let SkillsRouter = express.Router()
require("./routes/skillsRoute")(SkillsRouter);
app.use(config.rootAPI + 'skills', SkillsRouter)

// Setup path for all learners routes
let LearnersRouter = express.Router()
require("./routes/learnersRoute")(LearnersRouter);
app.use(config.rootAPI + 'learners', LearnersRouter)

// Setup path for all school years routes
let SchoolYearsRouter = express.Router()
require("./routes/schoolYearsRoute")(SchoolYearsRouter);
app.use(config.rootAPI + 'schoolyears', SchoolYearsRouter)

// Setup path for all calls routes
let CallsRouter = express.Router()
require("./routes/callsRoute")(CallsRouter);
app.use(config.rootAPI + 'calls', CallsRouter)



// Login route with user saving in session
app.post(config.rootAPI + 'login', (req, res) => {
    new Promise((resolve, reject) => {

        return auth.login(req, res)
            .then((result) => {

                if (!isErr(result)) {
                    req.session.user = result
                    resolve(res.json(checkAndChange(result)))
                } else {
                    reject(result)
                }
            })

    })
    .catch((err) => res.json(checkAndChange(err)))
})

// Logout route with destroy session
app.get(config.rootAPI + 'logout', (req, res) => {
                req.session.destroy()
                res.json(checkAndChange("disconnected"))
})

app.listen(config.port, () => console.log('Started on port ' + config.port))