module.exports = (sequelize, Sequelize) => {
    // Create call model to exports
    const Call = sequelize.define('call', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        date: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        morning: {
            type: Sequelize.TINYINT(1),
            allowNull: false
        },
        afternoon: {
            type: Sequelize.TINYINT(1),
            allowNull: false
        },
        lateness: {
            type: Sequelize.TINYINT(1),
            allowNull: false
        },
        comment: {
            type: Sequelize.TEXT,
            allowNull: true
        }
        
    }, {
        tableName: 'call',
        timestamps: false,
        underscored: true,
    })

    return Call;
}