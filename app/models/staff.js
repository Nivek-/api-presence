module.exports = (sequelize, Sequelize) => {
    // Create staff model to exports
    const Staff = sequelize.define('staff', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        firstname: {
            type: Sequelize.STRING(255),
            allowNull: false,
        },
        lastname: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        role: {
            type: Sequelize.TINYINT(1)
        },
        password: {
            type : Sequelize.STRING(255),
            allowNull: false
        }

    }, {
        tableName: 'staff',
        timestamps: false,
        underscored: true,
    })

    return Staff;
}