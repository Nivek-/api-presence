module.exports = (sequelize, Sequelize) => {
    // Create learner model to exports
    const Learner = sequelize.define('learner', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        firstname: {
            type: Sequelize.STRING(255),
            allowNull: false,
        },
        lastname: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        birthdate: {
            type: Sequelize.INTEGER(8),
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        on_training: {
            type: Sequelize.TINYINT(1),
            allowNull: false
        },
        close_reason: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        password: {
            type : Sequelize.STRING(255),
            allowNull: false
        }
    }, {
        tableName: 'learner',
        timestamps: false,
        underscored: true,
    })

    return Learner;
}