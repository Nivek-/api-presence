module.exports = (sequelize, Sequelize) => {
    // Create school year model to exports
    const SchoolYear = sequelize.define('school_year', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: false,
        },
        start_date: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        end_date: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        number: {
            type: Sequelize.BIGINT,
            allowNull: true
        }
    }, {
        tableName: 'school_year',
        timestamps: false,
        underscored: true
    })

    return SchoolYear;
}