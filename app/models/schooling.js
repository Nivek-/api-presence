module.exports = (sequelize, Sequelize) => {
    // Create schooling model to exports
    const Schooling = sequelize.define('schooling', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: false,
        }
    }, {
        tableName: 'schooling',
        timestamps: false,
        underscored: true
    })

    return Schooling;
}