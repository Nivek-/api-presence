module.exports = (sequelize, Sequelize) => {
    // Create forge model to exports
    const Forge = sequelize.define('forge', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: false,
        }
    }, {
        tableName: 'forge',
        timestamps: false,
        underscored: true,
    })

    return Forge;
}