module.exports = (sequelize, Sequelize) => {
    // Create staff model to exports
    const Skill = sequelize.define('skill', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: false,
        },
    }, {
        tableName: 'skill',
        timestamps: false,
        underscored: true,
    })

    return Skill;
}