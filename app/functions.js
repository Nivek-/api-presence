// Function to show result if success
exports.success = (result) => {
    return {
        status: 'success',
        result: result
    }
}

// Function to show error message if error
exports.error = (message) => {
    return {
        status: 'error',
        message: message
    }
}

// Function to verify if element is an object Error
exports.isErr = (err) => {
    return err instanceof Error
}

// Function with previous function to use more easily in differents files
exports.checkAndChange = (obj) => {
    if (this.isErr(obj)) {
        return this.error(obj.message)
    } else {
        return this.success(obj)
    }
}